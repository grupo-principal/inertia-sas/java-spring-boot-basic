package com.basic.springboot.javaspringbootbasic.component;

import org.springframework.stereotype.Component;
import org.springframework.util.SystemPropertyUtils;

@Component
public class ComponentTwoImplement implements ComponentDependency {
    @Override
    public void saludar() {
        System.out.println("Hola mundo desde mi componente 2");
    }
}
