package com.basic.springboot.javaspringbootbasic.bean;

public interface MyOperation {
    int sum(int number);
}
