package com.basic.springboot.javaspringbootbasic.bean;

public interface MyBeanWithDependency {
    void printWithDependency();
}
