package com.basic.springboot.javaspringbootbasic.caseuse;

import com.basic.springboot.javaspringbootbasic.entity.User;

import java.util.List;

public interface GetUser {
    List<User> getAll();
}
