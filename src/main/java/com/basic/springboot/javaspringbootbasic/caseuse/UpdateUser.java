package com.basic.springboot.javaspringbootbasic.caseuse;

import com.basic.springboot.javaspringbootbasic.entity.User;
import com.basic.springboot.javaspringbootbasic.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class UpdateUser {
    private UserService userService;

    public UpdateUser(UserService userService) {
        this.userService = userService;
    }

    public User update(User newUser, Long id) {
        return userService.update(newUser, id);
    }
}
