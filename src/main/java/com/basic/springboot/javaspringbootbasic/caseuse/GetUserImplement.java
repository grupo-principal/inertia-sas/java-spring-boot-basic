package com.basic.springboot.javaspringbootbasic.caseuse;

import com.basic.springboot.javaspringbootbasic.entity.User;
import com.basic.springboot.javaspringbootbasic.service.UserService;

import java.util.List;

public class GetUserImplement implements GetUser{
    private UserService userService;

    public GetUserImplement(UserService userService) {
        this.userService = userService;
    }

    @Override
    public List<User> getAll() {
        return userService.getAllUsers();
    }
}
