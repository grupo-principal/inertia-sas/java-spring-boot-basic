package com.basic.springboot.javaspringbootbasic.caseuse;

import com.basic.springboot.javaspringbootbasic.entity.User;
import com.basic.springboot.javaspringbootbasic.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class CreateUser {
    private UserService userService;

    public CreateUser(UserService userService) {
        this.userService = userService;
    }

    public User save(User newUser) {
        return userService.save(newUser);
    }
}
