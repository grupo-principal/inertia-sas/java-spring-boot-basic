package com.basic.springboot.javaspringbootbasic.configuration;

import com.basic.springboot.javaspringbootbasic.caseuse.GetUser;
import com.basic.springboot.javaspringbootbasic.caseuse.GetUserImplement;
import com.basic.springboot.javaspringbootbasic.service.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CaseUseConfiguration {

    @Bean
    GetUser getUser(UserService userService){
        return new GetUserImplement(userService);
    }
}
